#!/usr/bin/env python
# -*- coding: utf-8 -*-

from subprocess import call
from optparse import OptionParser
import shutil
from xml.dom import minidom
import os
import os.path
import classContainer
from smaliReader import SmaliReader
from permissionsManager import PermissionsManager
from functionManager import FunctionManager
from smaliExtractor import SmaliExtractor
import shutil

class Analyse:
	def __init__(self, apkPath):
		self.apkPath = apkPath
		self.permissions = [] # Tableau contenant les permissions
		self.services = [] # tableau pour les services
		self.contentProviders = [] # idem
		self.activities = [] # idem
		self.champsEtMethodes = {} # tableau de ChampsEtMethodes
		self.urls = []
		self.testDot = {}

	def analyseManifest(self):
		xmldoc = minidom.parse('tmp/AndroidManifest.xml')

		##########################
		# Package if exist.
		##########################
		manifest = xmldoc.getElementsByTagName('manifest')
		package = manifest[0].attributes['package'].value

		##########################
		# Parcours des activities
		##########################
		itemlist = xmldoc.getElementsByTagName('activity')

		for s in itemlist :
			itents = s.getElementsByTagName('intent-filter')
			tupleItent = []
			for itent in itents:
				action = s.getElementsByTagName('action')
				tupleItent.append(action[0].attributes['android:name'].value)
			activityName = s.attributes['android:name'].value
			if package not in activityName:
				activityName = package + activityName
			self.activities.append((activityName, tupleItent))

		##########################
		# Parcours des permissions
		##########################
		permissions = xmldoc.getElementsByTagName('uses-permission')
		for per in permissions:
			name = per.attributes['android:name'].value
			self.permissions.append(name)


		##########################
		# Parcours des services
		##########################
		services = xmldoc.getElementsByTagName('service')
		for ser in services:
			servname=ser.attributes['android:name'].value
			self.services.append(servname)

		##########################
		# Parcours des content
		##########################
		providers = xmldoc.getElementsByTagName('provider')
		for provider in providers:
			provname=provider.attributes['android:name'].value
			self.contentProviders.append(provname)


	def getActivities(self):
		if len(self.activities) == 0:
			self.analyseManifest()
		return self.activities

	def getMainActivity(self):
		if len(self.activities) == 0:
			self.analyseManifest()
			
		for activity in self.activities:
			if "android.intent.action.MAIN" in activity[1]:
				return activity[0]
		return None


	def listCalledMethodes(self):
		if len(self.champsEtMethodes) == 0:
			self.listChampsEtMethodes()
		SmaliReader.readDir('', SmaliExtractor.extractCalledMethode, self.champsEtMethodes, self.testDot)
		return self.testDot

	def listChampsEtMethodes(self):
		SmaliReader.readDir('', SmaliExtractor.extractMethode, None, self.champsEtMethodes)
		return self.champsEtMethodes

	def removePermission(self, permissionsToRemove): # ex : self.permissions[5]
		SmaliReader.readDir('', PermissionsManager.removePerm, permissionsToRemove, None)
		os.remove("tmp/permissionsLines")

	def replaceFunction(self, functionToReplace):
		SmaliReader.readDir('', FunctionManager.replaceFunction, functionToReplace, None)
		if not os.path.isfile("tmp/permissionsLines"):
			shutil.copytree("core/Lmyandroid","tmp/smali/myandroid" )

	def listUrls(self):
		SmaliReader.readDir('', SmaliExtractor.extractUrls, None, self.urls)
		return ""

	def methodesToDot(self):
		num = []
		outFile = file("out.dot", "w")
		outFile.write("digraph mon_graphe {\n")
		if len(self.testDot) == 0:
			self.listCalledMethodes()
		for k,v in self.testDot.iteritems():
			k = k.strip()
			if k not in num:
				num.append(k)
				outFile.write("\t\t" + str(num.index(k))+' [label="'+k+'"]\n')

			for val in v:
				val = val.strip()
				if val not in num:
					num.append(val)
					outFile.write("\t\t" + str(num.index(val))+' [label="'+val+'"]\n')

				outFile.write("\t" + str(num.index(k)) + "->" + str(num.index(val)) + ";\n")
		outFile.write("}")
		outFile.close()

	def getServices(self):
		if len(self.permissions) == 0:
			self.analyseManifest()
		return self.services

	def getPermissions(self):
		if len(self.permissions) == 0:
			self.analyseManifest()
		return self.permissions
