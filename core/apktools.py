#!/usr/bin/env python
# -*- coding: utf-8 -*-

from subprocess import call, Popen
from optparse import OptionParser
import shutil
from xml.dom import minidom
import os
import os.path
import classContainer
import api_permissions

class ApkTools:
	@staticmethod
	def unpackApk(apkPath):

		if(os.path.exists('tmp/')):
			shutil.rmtree('tmp/')
		os.mkdir('tmp/')
		if(call(['java', '-jar', os.path.dirname(os.path.realpath(__file__)) + "/../" + 'lib/apktool_2.0.0rc3.jar', 'd', '-f', apkPath ,'tmp/']) == 1):
			call(['java', '-jar', os.path.dirname(os.path.realpath(__file__)) + "/../" + 'lib/apktool_2.0.0rc3.jar', 'd', '-f', apkPath , '-o','tmp/'])			
	
	@staticmethod
	def packApk(certificate, install):
		if(os.path.exists('tmp/')):
			#print 'java'+ " " + '-jar'+ " " + os.path.dirname(os.path.realpath(__file__)) + " " + "/../" + " " + 'lib/apktool_2.0.0rc3.jar'+ " " + 'b' + " " +'tmp/'+ " " + 'out.apk'
			call(['java', '-jar', os.path.dirname(os.path.realpath(__file__)) + "/../" + 'lib/apktool_2.0.0rc3.jar', 'b' ,'tmp/', '-o', 'out.apk'])
			if(certificate == True):
				call(['jarsigner', '-keystore', os.path.expanduser('~/.android/debug.keystore'), '-storepass', 'android', '-keypass', 'android', 'out.apk', 'androiddebugkey', '-digestalg', 'SHA1', '-sigalg', 'MD5withRSA'])
			if(install == True):
				call(['adb', 'install', '-r', 'out.apk'])