#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path

class SmaliReader:
	@staticmethod
	def _readSmali(smali, mmethode, params, retourned):
		if(".smali" in smali):
			smaliFic = open(smali, 'r')
			lines = smaliFic.readlines()
			smaliFic.close()
			mmethode(lines, smali, params, retourned)

	@staticmethod
	def readDir(sdirectory, methode, params, retourned):
		dirs = os.listdir('tmp/smali/' + sdirectory)
		for directory in dirs:
			if os.path.isfile('tmp/smali/' + sdirectory + '/'+ directory):
				SmaliReader._readSmali('tmp/smali/' + sdirectory + '/'+ directory, methode, params, retourned)
			else:
				SmaliReader.readDir(sdirectory+'/'+directory, methode, params, retourned)