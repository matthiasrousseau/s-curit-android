#!/usr/bin/env python
# -*- coding: utf-8 -*-

from insertion import Insertion

class Api(Insertion):

	def __init__(self, nPackage, oPackage="Lfr/test/apicalling/"):
		Insertion.__init__(self, nPackage, oPackage)

	def getCodeClass(self):
		return ["smali/CallApi.smali","smali/CallApi$RetrieveAPI.smali"]

	def getOnCreate(self):
		return ""

	def getPermissionManifest(self):
		permission = []
		permission.append("android.permission.GET_ACCOUNTS")
		permission.append("android.permission.AUTHENTICATE_ACCOUNTS")
		return permission

	def getIntentManifest(self):
		return None