#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Insertion:

	def __init__(self, nPackage, oPackage):
		self.oldPackage = oPackage
		self.newPackage = nPackage

	def getCodeClass(self):
		raise NotImplementedError("Subclass must implement abstract method")

	def getOnCreate(self):
		raise NotImplementedError("Subclass must implement abstract method")

	def getPermissionManifest(self):
		raise NotImplementedError("Subclass must implement abstract method")

	# A inserer dans la balise application du manifest.
	def getIntentManifest(self):
		raise NotImplementedError("Subclass must implement abstract method")

	def getOldPackage(self):
		return self.oldPackage