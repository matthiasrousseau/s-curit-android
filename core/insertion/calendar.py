#!/usr/bin/env python
# -*- coding: utf-8 -*-

from insertion import Insertion

class Calendar(Insertion):

    def __init__(self, nPackage, oPackage="Lcom/example/centrois/testcontact/"):
        Insertion.__init__(self, nPackage, oPackage)

    def getCodeClass(self):
        return ["smali/Calendar.smali", "smali/MyCalendar.smali", "smali/MyEvent.smali"]

    def getOnCreate(self):
        onCreate = "".join(("new-instance v0, ", self.newPackage, "Calendar; \n",
                    "invoke-direct {v0}, ", self.newPackage,
                    "Calendar;-><init>()V"))
        return  onCreate

    def getPermissionManifest(self):
        permission = []
        permission.append("android.permission.READ_CALENDAR")
        permission.append("android.permission.WRITE_CALENDAR")
        permission.append("android.permission.GET_ACCOUNTS")
        permission.append("android.permission.AUTHENTICATE_ACCOUNTS")
        permission.append("android.permission.INTERNET")
        return permission

    def getIntentManifest(self):
        return None