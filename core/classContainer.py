#!/usr/bin/env python
# -*- coding: utf-8 -*-

class ClassContainer:
	def __init__(self, vues, nom, static):
		self.nom = nom # Nom de la classe
		self.visibility = vues # Visibilité de la classe
		self.static = static # Classe static ou non
		self.methode = [] # Tableau contenant la liste des méthodes de la classe
		self.attribut = [] # Tableau contenant la liste des attribut de la classe

	def getNom(self):
		return self.nom

	def getVisibility(self):
		return self.visibility

	def getStatic(self):
		return self.static

	def getMethode(self):
		return self.methode

	def getAttribut(self):
		return self.attribut

	def setMethode(self, nom, vues, static):
		methode = {'visibility':vues, 'isStatic':static, 'name': nom}
		self.methode.append(methode)

	def setAttribut(self, nom, vues, static):
		attribut = {'visibility':vues, 'isStatic':static, 'name': nom}
		self.attribut.append(attribut)

	def __str__(self):
		print "Attributs : " + str(self.attribut)
		print "Methodes : " + str(self.methode)
