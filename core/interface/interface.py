# -*- coding: utf-8 -*-

from Tkinter import *
from tkFileDialog import *
from decompilation import Decompilation
import sys
sys.path.append("../../core");
from analyse import Analyse;
from espion import Espion;
from permissions import Permissions;
from generate import Generate;
import time


class Interface(Frame):

	
	def __init__(self, fenetre, **kwargs):
		Frame.__init__(self,fenetre,**kwargs);
		self.root = fenetre;
		self.pack();
	
		fenetre.title("Debut Projet Sécurité Nomade");
		fenetre.geometry("400x300");

		self.message = Label(self,text="Bienvenue ...");
		self.message.grid(row=0,sticky =W);

		self.blanc = Label(self,text=" ");
		self.blanc.grid(row=1,sticky =W);

		self.bouton2 = Button(self,text="Decompilation",command=self.decompilation);
		self.bouton2.grid(row=2,sticky =W);

		self.bouton4 = Button(self,text="Analyse",command=self.analyse);
		self.bouton4.grid(row=3,sticky =W);

		self.bouton5 = Button(self,text="Ajouter code \"Espion\"",command=self.espion);
		self.bouton5.grid(row=4,sticky =W);
		self.bouton5.config(state=DISABLED);

		self.bouton6 = Button(self,text="Suppresion permissions",command=self.permissions);
		self.bouton6.grid(row=5,sticky =W);
		self.bouton6.config(state=DISABLED);

		self.bouton7 = Button(self,text="Generer APK",command=self.generate);
		self.bouton7.grid(row=6,sticky =W);

		self.bouton8 = Button(self,text="Quitter",command=fenetre.destroy);
		self.bouton8.grid(row=7,sticky =W);

		fenetre.mainloop();

	def decompilation(self):
		self.root.withdraw();
		d = Toplevel();
		interface2 = Decompilation(d);
		interface2.getTkinter(self.root);

	def analyse(self):
		fichier = askopenfilename(filetypes = [("Extension .apk", "*.apk")]);
		if (fichier != ''):
			ana = Analyse(fichier);
			ana.analyseManifest();
			ana.methodesToDot();
			ana.listUrls();
			print "O: generation du out.dot ! "

	def espion(self):
		self.root.withdraw();
		esp = Toplevel();
		interface_espion = Espion(esp);
		interface_espion.getTkinter(self.root);

	def permissions(self):
		self.root.withdraw();
		p = Toplevel();
		interface_permissions = Permissions(p);
		interface_permissions.getTkinter(self.root);

	def generate(self):
		self.root.withdraw();
		p = Toplevel()
		generate_apk = Generate(p)
		generate_apk.getTkinter(self.root);


