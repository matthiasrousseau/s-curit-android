# -*- coding: utf-8 -*-

from Tkinter import *
from tkFileDialog import *
from injection import Injection
class Espion(Frame):

	
	def __init__(self, fenetre, **kwargs):
		Frame.__init__(self,fenetre,**kwargs);
		self.pack();

		fenetre.title("Debut Projet Sécurité Nomade");
		fenetre.geometry("400x300");

		self.message = Label(self,text="Ajout de code espion");
		self.message.grid(row=0,sticky =W);

		global gps;
		global appel;
		global sms;	

		global injection
		injection  = Injection()
		
		gps = IntVar();
		appel = IntVar();
		sms = IntVar();
		
		Checkbutton(self, text="Ajout GPS", variable=gps).grid(row=1, sticky=W);
		
		Checkbutton(self, text="Ajout Appel", variable=appel).grid(row=2, sticky=W);
		
		Checkbutton(self, text="Ajout SMS", variable=sms).grid(row=3, sticky=W);

		self.bouton2 = Button(self,text="Show",command=self.resultat([],injection,[]));
		self.bouton2.grid(row=4,sticky =W);

		self.bouton3 = Button(self,text="Quitter",command=self.close);
		self.bouton3.grid(row=5,sticky =W);

		self.toplevel = fenetre;

	def resultat(self,permissionAdded,injection,analyse):
		print("gps : %d , appel : %d, sms %d \n"% (gps.get(),appel.get(),sms.get()));
		if(gps.get()==1):
			Injection.addFunctionality("gps")
			permissionAdded.append("gps")
		if(appel.get()==1):
			Injection.addFunctionality("appel")
			permissionAdded.append("appel")
		if(sms.get()==1):
			Injection.addFunctionality("sms")
			permissionAdded.append("sms")



	def getTkinter(self,op):
		self.leParent = op;

	def close(self):
		self.leParent.deiconify();
		self.toplevel.destroy();

