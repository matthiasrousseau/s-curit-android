# -*- coding: utf-8 -*-

from Tkinter import *
from tkFileDialog import *
from decompilation import Decompilation
import sys
sys.path.append("../../core");
from analyse import Analyse;
from espion import Espion;
from permissions import Permissions;
import time
from apktools import ApkTools;


class Generate(Frame):

	
	def __init__(self, fenetre, **kwargs):
		Frame.__init__(self,fenetre,**kwargs);
		self.root = fenetre;
		self.pack();

		global certificate;
		global install;
		
		certificate = IntVar();
		install = IntVar();
		
		Checkbutton(self, text="Certificate ?", variable=certificate).grid(row=2, sticky=W);
		
		Checkbutton(self, text="Install ? ", variable=install).grid(row=3, sticky=W);
		
	
		fenetre.title("Debut Projet Sécurité Nomade");
		fenetre.geometry("400x300");

		self.message = Label(self,text="Generate APK : Options");
		self.message.grid(row=0,sticky =W);

		self.blanc = Label(self,text=" ");
		self.blanc.grid(row=1,sticky =W);

		self.bouton2 = Button(self,text="Go Generate",command=self.generate);
		self.bouton2.grid(row=4,sticky =W);

		self.bouton2 = Button(self,text="Retour",command=self.close);
		self.bouton2.grid(row=5,sticky =W);

		self.toplevel = fenetre;

	def getTkinter(self,op):
		self.leParent = op;

	def generate(self):
		varC = False
		varI = False
		if (certificate.get()==1):
			varC = True
		if (install.get()==1):
			varI = True
		ApkTools.packApk(certificate, install)
		print "out.apk généré !"

	def close(self):
		self.leParent.deiconify();
		self.toplevel.destroy();

		

