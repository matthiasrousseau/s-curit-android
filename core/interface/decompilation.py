# -*- coding: utf-8 -*-

from Tkinter import *
from tkFileDialog import *
import sys
sys.path.append("../../core");
from apktools import ApkTools;

class Decompilation(Frame):

	
	
	def __init__(self, fenetre, **kwargs):
		Frame.__init__(self,fenetre,**kwargs);
		
		self.pack();

		fenetre.title("Decompilation");
		fenetre.geometry("400x300");

		self.message = Label(self,text="SELECTIONNER L\'APK :");
		self.message.grid(row=0,column=0,sticky =W);

		self.bouton1 = Button(self,text="...",command=self.selectApk);
		self.bouton1.grid(row=0,column=1,sticky =W);

		self.le_path = Label(self,text="...");
		self.le_path.grid(row=1,column=0,sticky =W);

		self.bouton2 = Button(self,text="Decompilation",command=self.decompil);
		self.bouton2.grid(row=1,column=1,sticky =W);
		self.bouton2.config(state=DISABLED);

		self.bouton3 = Button(self,text="Fermer",command=self.close);
		self.bouton3.grid(row=2,column=1,sticky =W);

		self.toplevel = fenetre;

	def selectApk(self):
		f = askopenfilename(filetypes = [("Extension .apk", "*.apk")]);
		self.le_path.config(text=f);
		if (f != ''):
			self.bouton2.config(state=NORMAL);

	def decompil(self):
		self.bouton3.config(state=DISABLED);
		path = self.le_path.cget("text");
		print path;
		apk = ApkTools;
		apk.unpackApk(path);
		print "O: Décompression terminée";
		self.bouton3.config(state=NORMAL);

	def getTkinter(self,op):
		self.leParent = op;
		print "here"

	def close(self):
		self.leParent.deiconify();
		self.toplevel.destroy();



		



