.class public Lcom/example/centrois/testcontact/MyCalendar;
.super Ljava/lang/Object;
.source "MyCalendar.java"


# instance fields
.field private accountName:Ljava/lang/String;

.field private calID:J

.field private displayName:Ljava/lang/String;

.field private ownerName:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "calID"    # J
    .param p3, "ownerName"    # Ljava/lang/String;
    .param p4, "accountName"    # Ljava/lang/String;
    .param p5, "displayName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/example/centrois/testcontact/MyCalendar;->calID:J

    .line 9
    iput-object v2, p0, Lcom/example/centrois/testcontact/MyCalendar;->displayName:Ljava/lang/String;

    .line 10
    iput-object v2, p0, Lcom/example/centrois/testcontact/MyCalendar;->accountName:Ljava/lang/String;

    .line 11
    iput-object v2, p0, Lcom/example/centrois/testcontact/MyCalendar;->ownerName:Ljava/lang/String;

    .line 14
    iput-wide p1, p0, Lcom/example/centrois/testcontact/MyCalendar;->calID:J

    .line 15
    iput-object p3, p0, Lcom/example/centrois/testcontact/MyCalendar;->ownerName:Ljava/lang/String;

    .line 16
    iput-object p4, p0, Lcom/example/centrois/testcontact/MyCalendar;->accountName:Ljava/lang/String;

    .line 17
    iput-object p5, p0, Lcom/example/centrois/testcontact/MyCalendar;->displayName:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public getAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/example/centrois/testcontact/MyCalendar;->accountName:Ljava/lang/String;

    return-object v0
.end method

.method public getCalID()J
    .locals 2

    .prologue
    .line 22
    iget-wide v0, p0, Lcom/example/centrois/testcontact/MyCalendar;->calID:J

    return-wide v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/example/centrois/testcontact/MyCalendar;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getOwnerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/example/centrois/testcontact/MyCalendar;->ownerName:Ljava/lang/String;

    return-object v0
.end method

.method public setAccountName(Ljava/lang/String;)V
    .locals 0
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/example/centrois/testcontact/MyCalendar;->accountName:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public setCalID(J)V
    .locals 1
    .param p1, "calID"    # J

    .prologue
    .line 26
    iput-wide p1, p0, Lcom/example/centrois/testcontact/MyCalendar;->calID:J

    .line 27
    return-void
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/example/centrois/testcontact/MyCalendar;->displayName:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public setOwnerName(Ljava/lang/String;)V
    .locals 0
    .param p1, "ownerName"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/example/centrois/testcontact/MyCalendar;->ownerName:Ljava/lang/String;

    .line 51
    return-void
.end method
