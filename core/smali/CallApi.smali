.class public Lfr/test/apicalling/CallApi;
.super Ljava/lang/Thread;
.source "CallApi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lfr/test/apicalling/CallApi$RetrieveAPI;
    }
.end annotation


# static fields
.field private static final API_URL:Ljava/lang/String; = "http://URL_HERE/add"


# instance fields
.field private url:Ljava/lang/String;


# direct methods
.method public varargs constructor <init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "Type"    # Ljava/lang/String;
    .param p3, "params"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 15
    const-string v4, ""

    iput-object v4, p0, Lfr/test/apicalling/CallApi;->url:Ljava/lang/String;

    .line 19
    const-string v4, "plop"

    const-string v5, "CallApi"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 20
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "http://URL_HERE/add?0="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lfr/test/apicalling/CallApi;->url:Ljava/lang/String;

    .line 21
    const-string v2, "Inconnu"

    .line 22
    .local v2, "email":Ljava/lang/String;
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    .line 24
    .local v1, "accounts":[Landroid/accounts/Account;
    array-length v5, v1

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v5, :cond_0

    .line 31
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lfr/test/apicalling/CallApi;->url:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "&1="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lfr/test/apicalling/CallApi;->url:Ljava/lang/String;

    .line 32
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    array-length v4, p3

    if-lt v3, v4, :cond_2

    .line 35
    return-void

    .line 24
    .end local v3    # "i":I
    :cond_0
    aget-object v0, v1, v4

    .line 25
    .local v0, "account":Landroid/accounts/Account;
    iget-object v6, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v7, "com.google"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 26
    iget-object v2, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 27
    goto :goto_1

    .line 24
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 33
    .end local v0    # "account":Landroid/accounts/Account;
    .restart local v3    # "i":I
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lfr/test/apicalling/CallApi;->url:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v3, 0x2

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, p3, v3

    const-string v6, "UTF-8"

    invoke-static {v5, v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lfr/test/apicalling/CallApi;->url:Ljava/lang/String;

    .line 32
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method static synthetic access$0(Lfr/test/apicalling/CallApi;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lfr/test/apicalling/CallApi;->url:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 40
    :try_start_0
    new-instance v0, Lfr/test/apicalling/CallApi$RetrieveAPI;

    invoke-direct {v0, p0}, Lfr/test/apicalling/CallApi$RetrieveAPI;-><init>(Lfr/test/apicalling/CallApi;)V

    .line 41
    .local v0, "api":Lfr/test/apicalling/CallApi$RetrieveAPI;
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lfr/test/apicalling/CallApi;->url:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lfr/test/apicalling/CallApi$RetrieveAPI;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    .end local v0    # "api":Lfr/test/apicalling/CallApi$RetrieveAPI;
    :goto_0
    return-void

    .line 42
    :catch_0
    move-exception v1

    goto :goto_0
.end method
