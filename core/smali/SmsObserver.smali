.class public Lcom/example/smstest/SmsObserver;
.super Ljava/lang/Object;
.source "SmsObserver.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Lcom/example/smstest/SmsSend;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, v1}, Lcom/example/smstest/SmsSend;-><init>(Landroid/os/Handler;)V

    .local v0, "observer":Landroid/database/ContentObserver;
    move-object v1, v0

    .line 11
    check-cast v1, Lcom/example/smstest/SmsSend;

    invoke-virtual {v1, p1}, Lcom/example/smstest/SmsSend;->setContext(Landroid/content/Context;)V

    .line 12
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 13
    const-string v2, "content://sms"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x1

    .line 12
    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 14
    return-void
.end method
