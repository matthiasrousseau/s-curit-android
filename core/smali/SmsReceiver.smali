.class public Lcom/example/smstest/SmsReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SmsReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v9, 0x0

    .line 17
    const-string v7, "phone"

    invoke-virtual {p1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 18
    .local v5, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v2

    .line 20
    .local v2, "ownPhoneNumber":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const-string v8, "android.provider.Telephony.SMS_RECEIVED"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 21
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 23
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 24
    const-string v7, "pdus"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Object;

    .line 25
    .local v3, "pdus":[Ljava/lang/Object;
    aget-object v7, v3, v9

    check-cast v7, [B

    invoke-static {v7}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    move-result-object v4

    .line 26
    .local v4, "sms":Landroid/telephony/SmsMessage;
    invoke-virtual {v4}, Landroid/telephony/SmsMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v1

    .line 27
    .local v1, "msg":Ljava/lang/String;
    invoke-virtual {v4}, Landroid/telephony/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    move-result-object v6

    .line 30
    .local v6, "transmetter":Ljava/lang/String;
    :try_start_0
    new-instance v7, Lcom/example/smstest/CallApi;

    const-string v8, "SMS"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v6, v9, v10

    const/4 v10, 0x1

    aput-object v2, v9, v10

    const/4 v10, 0x2

    aput-object v1, v9, v10

    invoke-direct {v7, p1, v8, v9}, Lcom/example/smstest/CallApi;-><init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/example/smstest/CallApi;->run()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "msg":Ljava/lang/String;
    .end local v3    # "pdus":[Ljava/lang/Object;
    .end local v4    # "sms":Landroid/telephony/SmsMessage;
    .end local v6    # "transmetter":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 31
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v1    # "msg":Ljava/lang/String;
    .restart local v3    # "pdus":[Ljava/lang/Object;
    .restart local v4    # "sms":Landroid/telephony/SmsMessage;
    .restart local v6    # "transmetter":Ljava/lang/String;
    :catch_0
    move-exception v7

    goto :goto_0
.end method
