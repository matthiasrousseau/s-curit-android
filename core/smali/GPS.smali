.class public Lfr/project/gpstest/GPS;
.super Landroid/app/Service;
.source "GPS.java"

# interfaces
.implements Landroid/location/LocationListener;


# static fields
.field private static final MIN_DISTANCE_CHANGE_FOR_UPDATES:J = 0x1L

.field private static final MIN_TIME_BW_UPDATES:J = 0x3e8L


# instance fields
.field private isGPSEnabled:Z

.field private location:Landroid/location/Location;

.field private locationManager:Landroid/location/LocationManager;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfr/project/gpstest/GPS;->isGPSEnabled:Z

    .line 30
    iput-object p1, p0, Lfr/project/gpstest/GPS;->mContext:Landroid/content/Context;

    .line 31
    invoke-virtual {p0}, Lfr/project/gpstest/GPS;->getLocation()Landroid/location/Location;

    .line 32
    return-void
.end method


# virtual methods
.method public getLocation()Landroid/location/Location;
    .locals 7

    .prologue
    .line 37
    :try_start_0
    iget-object v0, p0, Lfr/project/gpstest/GPS;->mContext:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lfr/project/gpstest/GPS;->locationManager:Landroid/location/LocationManager;

    .line 39
    iget-object v0, p0, Lfr/project/gpstest/GPS;->locationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lfr/project/gpstest/GPS;->isGPSEnabled:Z

    .line 40
    iget-boolean v0, p0, Lfr/project/gpstest/GPS;->isGPSEnabled:Z

    if-nez v0, :cond_0

    .line 41
    const/4 v0, 0x0

    .line 59
    :goto_0
    return-object v0

    .line 44
    :cond_0
    iget-object v0, p0, Lfr/project/gpstest/GPS;->location:Landroid/location/Location;

    if-nez v0, :cond_1

    .line 45
    iget-object v0, p0, Lfr/project/gpstest/GPS;->locationManager:Landroid/location/LocationManager;

    .line 46
    const-string v1, "gps"

    .line 47
    const-wide/16 v2, 0x3e8

    .line 48
    const/high16 v4, 0x3f800000

    move-object v5, p0

    .line 45
    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 49
    iget-object v0, p0, Lfr/project/gpstest/GPS;->locationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_1

    .line 50
    iget-object v0, p0, Lfr/project/gpstest/GPS;->locationManager:Landroid/location/LocationManager;

    .line 51
    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 50
    iput-object v0, p0, Lfr/project/gpstest/GPS;->location:Landroid/location/Location;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :cond_1
    :goto_1
    iget-object v0, p0, Lfr/project/gpstest/GPS;->location:Landroid/location/Location;

    goto :goto_0

    .line 55
    :catch_0
    move-exception v6

    .line 56
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 87
    const/4 v0, 0x0

    return-object v0
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 8
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 64
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v1

    .line 65
    .local v1, "latitude":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    .line 66
    .local v2, "longitude":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, "altitude":Ljava/lang/String;
    :try_start_0
    new-instance v3, Lfr/project/gpstest/CallApi;

    iget-object v4, p0, Lfr/project/gpstest/GPS;->mContext:Landroid/content/Context;

    const-string v5, "GPS"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    const/4 v7, 0x1

    aput-object v2, v6, v7

    const/4 v7, 0x2

    aput-object v0, v6, v7

    invoke-direct {v3, v4, v5, v6}, Lfr/project/gpstest/CallApi;-><init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v3}, Lfr/project/gpstest/CallApi;->run()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :goto_0
    return-void

    .line 69
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 83
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 79
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 75
    return-void
.end method
