.class Lfr/test/apicalling/CallApi$RetrieveAPI;
.super Landroid/os/AsyncTask;
.source "CallApi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lfr/test/apicalling/CallApi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RetrieveAPI"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lfr/test/apicalling/CallApi;


# direct methods
.method constructor <init>(Lfr/test/apicalling/CallApi;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lfr/test/apicalling/CallApi$RetrieveAPI;->this$0:Lfr/test/apicalling/CallApi;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lfr/test/apicalling/CallApi$RetrieveAPI;->doInBackground([Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p1, "urls"    # [Ljava/lang/String;

    .prologue
    .line 49
    :try_start_0
    new-instance v0, Ljava/net/URL;

    iget-object v2, p0, Lfr/test/apicalling/CallApi$RetrieveAPI;->this$0:Lfr/test/apicalling/CallApi;

    # getter for: Lfr/test/apicalling/CallApi;->url:Ljava/lang/String;
    invoke-static {v2}, Lfr/test/apicalling/CallApi;->access$0(Lfr/test/apicalling/CallApi;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 50
    .local v0, "_url":Ljava/net/URL;
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    .line 51
    .local v1, "urlConnection":Ljava/net/URLConnection;
    invoke-virtual {v1}, Ljava/net/URLConnection;->getContent()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    .end local v0    # "_url":Ljava/net/URL;
    .end local v1    # "urlConnection":Ljava/net/URLConnection;
    :goto_0
    const/4 v2, 0x0

    return-object v2

    .line 52
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "content"    # Ljava/lang/Object;

    .prologue
    .line 58
    return-void
.end method