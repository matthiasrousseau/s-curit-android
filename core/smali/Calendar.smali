.class public Lcom/example/centrois/testcontact/Calendar;
.super Ljava/lang/Object;
.source "Calendar.java"


# static fields
.field public static final EVENT_PROJECTION:[Ljava/lang/String;

.field private static final PROJECTION_ACCOUNT_NAME_INDEX:I = 0x1

.field private static final PROJECTION_DISPLAY_NAME_INDEX:I = 0x2

.field private static final PROJECTION_ID_INDEX:I = 0x0

.field private static final PROJECTION_OWNER_ACCOUNT_INDEX:I = 0x3


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "account_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "calendar_displayName"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "ownerAccount"

    aput-object v2, v0, v1

    sput-object v0, Lcom/example/centrois/testcontact/Calendar;->EVENT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCalendar(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/accounts/AccountManager;Landroid/content/Context;)V
    .locals 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cr"    # Landroid/content/ContentResolver;
    .param p3, "manager"    # Landroid/accounts/AccountManager;
    .param p4, "ctx"    # Landroid/content/Context;

    .prologue
    .line 37
    const/4 v15, 0x0

    .line 38
    .local v15, "cur":Landroid/database/Cursor;
    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    .line 39
    .local v3, "uri":Landroid/net/Uri;
    const-string v5, "((account_name = ?) AND (account_type = ?))"

    .line 43
    .local v5, "selection":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v13

    .line 45
    .local v13, "accountsOnDevice":[Landroid/accounts/Account;
    new-instance v18, Lcom/example/centrois/testcontact/MyEvent;

    invoke-direct/range {v18 .. v18}, Lcom/example/centrois/testcontact/MyEvent;-><init>()V

    .line 46
    .local v18, "myEvent":Lcom/example/centrois/testcontact/MyEvent;
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .local v14, "calendarAlreadyGet":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 50
    .local v17, "mc":Ljava/util/List;, "Ljava/util/List<Lcom/example/centrois/testcontact/MyCalendar;>;"
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_0
    array-length v2, v13

    move/from16 v0, v16

    if-ge v0, v2, :cond_2

    .line 51
    aget-object v2, v13, v16

    iget-object v2, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v4, "com.google"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 52
    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v4, v13, v16

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v6, v2

    const/4 v2, 0x1

    const-string v4, "com.google"

    aput-object v4, v6, v2

    .line 55
    .local v6, "selectionArgs":[Ljava/lang/String;
    sget-object v4, Lcom/example/centrois/testcontact/Calendar;->EVENT_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 58
    :goto_1
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 60
    new-instance v7, Lcom/example/centrois/testcontact/MyCalendar;

    const/4 v2, 0x0

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const/4 v2, 0x2

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v2, 0x1

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v2, 0x3

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct/range {v7 .. v12}, Lcom/example/centrois/testcontact/MyCalendar;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 63
    :cond_0
    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v14}, Lcom/example/centrois/testcontact/MyEvent;->readCalendarEvent(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v14

    .line 50
    .end local v6    # "selectionArgs":[Ljava/lang/String;
    :cond_1
    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    .line 69
    :cond_2
    const/16 v16, 0x0

    :goto_2
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v16

    if-ge v0, v2, :cond_3

    .line 71
    :try_start_0
    new-instance v4, Lcom/example/centrois/testcontact/CallApi;

    const-string v7, "CALENDAR"

    const/4 v2, 0x5

    new-array v8, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v9, "CALENDAR"

    aput-object v9, v8, v2

    const/4 v9, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/example/centrois/testcontact/MyCalendar;

    invoke-virtual {v2}, Lcom/example/centrois/testcontact/MyCalendar;->getCalID()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v9

    const/4 v9, 0x2

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/example/centrois/testcontact/MyCalendar;

    invoke-virtual {v2}, Lcom/example/centrois/testcontact/MyCalendar;->getAccountName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v9

    const/4 v9, 0x3

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/example/centrois/testcontact/MyCalendar;

    invoke-virtual {v2}, Lcom/example/centrois/testcontact/MyCalendar;->getOwnerName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v9

    const/4 v9, 0x4

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/example/centrois/testcontact/MyCalendar;

    invoke-virtual {v2}, Lcom/example/centrois/testcontact/MyCalendar;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v9

    move-object/from16 v0, p4

    invoke-direct {v4, v0, v7, v8}, Lcom/example/centrois/testcontact/CallApi;-><init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/example/centrois/testcontact/CallApi;->run()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 69
    :goto_3
    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    .line 76
    :cond_3
    const/16 v16, 0x0

    :goto_4
    invoke-virtual/range {v18 .. v18}, Lcom/example/centrois/testcontact/MyEvent;->getNbEvent()I

    move-result v2

    move/from16 v0, v16

    if-ge v0, v2, :cond_4

    .line 78
    :try_start_1
    new-instance v4, Lcom/example/centrois/testcontact/CallApi;

    const-string v7, "CALENDAR"

    const/4 v2, 0x6

    new-array v8, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v9, "EVENT"

    aput-object v9, v8, v2

    const/4 v9, 0x1

    invoke-virtual/range {v18 .. v18}, Lcom/example/centrois/testcontact/MyEvent;->getCalendarID()Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v10, v2

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v9

    const/4 v9, 0x2

    invoke-virtual/range {v18 .. v18}, Lcom/example/centrois/testcontact/MyEvent;->getNameOfEvent()Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v8, v9

    const/4 v9, 0x3

    invoke-virtual/range {v18 .. v18}, Lcom/example/centrois/testcontact/MyEvent;->getStartDates()Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v8, v9

    const/4 v9, 0x4

    invoke-virtual/range {v18 .. v18}, Lcom/example/centrois/testcontact/MyEvent;->getEndDates()Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v8, v9

    const/4 v9, 0x5

    invoke-virtual/range {v18 .. v18}, Lcom/example/centrois/testcontact/MyEvent;->getDescriptions()Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v10, "[\r\n]+"

    const-string v11, " "

    invoke-virtual {v2, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v9

    move-object/from16 v0, p4

    invoke-direct {v4, v0, v7, v8}, Lcom/example/centrois/testcontact/CallApi;-><init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/example/centrois/testcontact/CallApi;->run()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 81
    :goto_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v18 .. v18}, Lcom/example/centrois/testcontact/MyEvent;->getCalendarID()Ljava/util/ArrayList;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "|"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v18 .. v18}, Lcom/example/centrois/testcontact/MyEvent;->getNameOfEvent()Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "|"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v18 .. v18}, Lcom/example/centrois/testcontact/MyEvent;->getStartDates()Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "|"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v18 .. v18}, Lcom/example/centrois/testcontact/MyEvent;->getEndDates()Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "|"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v18 .. v18}, Lcom/example/centrois/testcontact/MyEvent;->getDescriptions()Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v7, "[\r\n]+"

    const-string v8, " "

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 76
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_4

    .line 85
    :cond_4
    return-void

    .line 80
    :catch_0
    move-exception v2

    goto :goto_5

    .line 73
    :catch_1
    move-exception v2

    goto/16 :goto_3
.end method
