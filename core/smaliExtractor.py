#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path
import api_permissions
import classContainer

class SmaliExtractor:
	@staticmethod
	def extractUrls(lines, smali, params, retourned):
		for line in [s for s in lines if "://" in s]:
			splited = line.split()
			if('"' in splited[2]):
				retourned.append(splited[2])
				print splited[2]

	@staticmethod
	def extractCalledMethode(lines, smali, params, retourned):
		classInfos = [s for s in lines if ".class" in s][0].split()
		tpackage = classInfos[-1:][0][:-1]
		tmethode = ""

		for line in lines:
			if(".method" in line):
				tmethode = line.split()[-1].split("(")[0]
			if("invoke" in line):
				called = line.split(",")[-1]
				package = called.split(";->")[0]
				if(package.strip() in params.keys()):
					if tpackage+"/"+tmethode not in retourned.keys():
						retourned[tpackage+"/"+tmethode] = []
					retourned[tpackage+"/"+tmethode].append(package+"/"+called.split(";->")[1].split('(')[0]);
		
	@staticmethod			
	def extractMethode(lines, smali, params, retourned):
		classInfos = [s for s in lines if ".class" in s][0].split()
		package = classInfos[-1:][0][:-1]
		second = None
		third = None
		if len(classInfos) >= 3:
			second = classInfos[1]
		if len(classInfos) == 4:
			third = classInfos[2]

		visibility = "protected"
		other = ""

		if second is not None:
			if second in ["public", "private", "protected"]:
				visibility = second
			elif third is not None and third in ["public", "private", "protected"]:
				visibility = third
			elif third is not None:
				other = third
			else:
				other = second

		myClass = classContainer.ClassContainer(visibility, package.split("/")[-1:][0], other)

		for line in [s for s in lines if ".method" in s]:
			splited = line.split()
			visibility = ""
			static = ""
			methodeName = ""
			if "static" in splited:
				static = "static"
			if(len(splited) == 2):
				visibility = "protected"
			else :
				visibility = splited[1]
			methodeName = splited[len(splited) -1 ].split("(")[0]
			myClass.setMethode(methodeName, visibility, static)
		retourned[package] = myClass