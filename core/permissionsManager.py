#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path
import api_permissions

class PermissionsManager:
	@staticmethod
	def removePerm(lines, smali, params, retourned):
		if not os.path.isfile("tmp/permissionsLines"):
			ofpl = file("tmp/permissionsLines","w")
			if params not in api_permissions.API_BY_PERMISSION.keys():
				return
			for data in api_permissions.API_BY_PERMISSION[params].keys():
				for a in api_permissions.API_BY_PERMISSION[params][data]:
					ofpl.write(a+"\n")
			ofpl.close()

		permFic = open("tmp/permissionsLines", 'r')
		permLines = permFic.readlines()
		
		permLinesAux = []

		for case in permLines:
			permLinesAux.append(case[:-1])

		of = file(smali+"2",'a')	

		for line in lines:
			#if line[:-1] not in permLinesAux:
			of.write(line)
			#else:
			print smali
			print line

		os.rename(smali+'2',smali)