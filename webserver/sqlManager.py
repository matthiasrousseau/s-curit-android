# -*- coding: utf-8 -*-
import sqlite3
import json

class SqlManager:
	def __init__(self):
		self.connection = sqlite3.connect("data.db", check_same_thread=False)
		self.cursor = self.connection.cursor()

		self._createDB()

	def _createDB(self):
		sql = 'create table if not exists SMS (user TEXT, sender TEXT, message TEXT)'
		self.cursor.execute(sql)

		sql = 'create table if not exists GPS (user TEXT, latitude REAL, longitude REAL, altitude REAL)'
		self.cursor.execute(sql)

		sql = 'create table if not exists CONTACTS (user TEXT, name TEXT, phone TEXT)'
		self.cursor.execute(sql)

		# A completer

		self.connection.commit()

	def insertSMS(self, receiver, sender, message):
		self.cursor.execute("INSERT INTO SMS VALUES(?,?,?)", [receiver, sender, message])
		self.connection.commit()

	def insertGPS(self, receiver, latitude, longitude, altitude):
		self.cursor.execute("INSERT INTO GPS VALUES(?,?,?,?)", [receiver, float(latitude), float(longitude), float(altitude)])
		self.connection.commit()

	def insertCONTACTS(self, receiver, name, phone):
		self.cursor.execute("INSERT INTO CONTACTS VALUES(?,?,?)", [receiver, name, phone])
		self.connection.commit()

	def selectSMS(self, receiver):
		self.cursor.execute('SELECT * FROM SMS WHERE user=?', [receiver])
		return self.cursor.fetchall()

	def selectGPS(self):
		self.cursor.execute('SELECT * FROM GPS')
		return json.dumps(self.cursor.fetchall())

	def selectGPS(self):
		self.cursor.execute('SELECT * FROM CONTACTS')
		return json.dumps(self.cursor.fetchall())