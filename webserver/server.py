# -*- coding: utf-8 -*-
import web
import json
import sqlite3
from sqlManager import SqlManager

urls = (
	'/sms', 'sms',
	'/location', 'location',
	'/calendar', 'calendar',
	'/contacts', 'contacts',
	'/storage', 'storage',
	'/storage', 'storage',
	'/add', 'add',
	'/', 'home'
)

dblocation = "data.db"

sql = SqlManager()

class add:
	def POST(self, datas):
		pass

	def GET(self, *args):
		values = sorted(web.input().items())
		typeData = values[0]
		values = [v for s,v in values[1:]]
		print str(typeData) + " --> " + str(values)
		if typeData[1].upper() == "GPS" and len(values) == 4:
			sql.insertGPS(*values)

		return ""

class sms:
	def POST(self, datas):
		pass

	def GET(self):
		pass

class location:
	def POST(self, datas):
		pass

	def GET(self):
		return sql.selectGPS()

class contacts:
	def POST(self, datas):
		pass

	def GET(self):
		pass

class storage:
	def POST(self, datas):
		pass

	def GET(self):
		pass


class home:
	def POST(self, datas):
		pass

	def GET(self):
  		pass


app = web.application(urls, globals())

if __name__ == "__main__":
	app.run()