#!/bin/bash

if [ $# -ne 1 ]; then
	echo "Usage : ./compilation.sh nom_apk.apk"
	exit
fi

apk=$1

jarsigner -verbose  -sigalg SHA1withRSA -digestalg SHA1 -keystore ~/.android/debug.keystore -storepass android -keypass android $apk androiddebugkey

result=`cat tmp/AndroidManifest.xml | egrep "package"`
line=""
for w in $result; do
	a=`echo $w | grep package`

	if [ -n "$a" ]; then
		line=$a
		break
	fi
done

echo $line | cut -d\" -f2
pack=`echo $line | cut -d\" -f2`

adb uninstall $pack
adb install $apk
